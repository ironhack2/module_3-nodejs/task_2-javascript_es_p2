import { Rectangle } from './Rectangle.js';
import { Square } from './Square.js';

// Khởi tạo đối tượng  của class Rectangle
let rectangle = new Rectangle(5, 10);
//Tính diện tích hình chữ nhật
console.log("Diện tích hình chữ nhật:", rectangle.getArea()); // 50


//Khởi tạo đối tượng của class Square
let square = new Square(5);
//Tinh chu vi
console.log("Chu vi hình vuông:", square.getPerimeter()); //20
//Tính diện tích hình vuông
console.log("Diện tích hình vuông:", square.getArea()); //25



//Kiểm tra xem đối tượng vừa tạo có đúng kiểu với class không ?
console.log(square instanceof Square); //true
console.log(square instanceof Rectangle); //true
console.log(rectangle instanceof Square); // false