class Rectangle {
    //Hàm tạo(constructor)
    constructor(length, width) {
        this.length = length;
        this.width = width;
    }

    //Phương thức của class
    getArea() {
        return this.length * this.width;
    }
}
export { Rectangle }