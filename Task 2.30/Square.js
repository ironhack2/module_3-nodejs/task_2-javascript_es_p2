import { Rectangle } from './Rectangle.js';

//Tạo class Square kế thừa class Rectangle
class Square extends Rectangle {
    // constructor của class con
    constructor(length) {
        //Gọi đến constructor của class cha
        super(length, length);
    }

    //Phương thức của class con
    getPerimeter() {
        return 2 * (this.length + this.width);
    }
}
export { Square }