import { Vehicle } from './js/vehicle.js'
import { Car } from './js/car.js'
import { Motorbike } from './js/motorbike.js'



    ////////////         VEHICLE        //////////////
    let vVehicle = new Vehicle("Honda", 1948);
    console.log("Vehicle: " + vVehicle.getPrint());



    ////////////         CAR        //////////////
    let vCar = new Car("Mazda", "2021", "premium", "CX-5");
    console.log("Car: " + vCar.getHonk());
    console.log(vCar instanceof Vehicle);



    ////////////         MOTORBIKE        //////////////
    let vMotorbike = new Motorbike("Honda", 2019, "150i","SH");
    console.log("Motorbike: " + vMotorbike.getHonk());
    console.log(vMotorbike instanceof Vehicle);