class Vehicle {
    constructor(paramBrand, paramYearManual) {
        this.brand = paramBrand;
        this.yearManual = paramYearManual;
    }
    getPrint() {
        return this.brand + " " + this.yearManual;
    }
}
export { Vehicle };