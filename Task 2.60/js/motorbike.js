import { Vehicle } from './vehicle.js'

class Motorbike extends Vehicle {
    constructor(paramBrand, paramYearManual, paramID, paramModelName) {
        super(paramBrand, paramYearManual)
        this.vId = paramID;
        this.modelName = paramModelName;
    }
    getHonk() {
        return this.brand + " " + this.yearManual + " " + this.vId + " " + this.modelName;
    }
}
export { Motorbike };